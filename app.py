from flask import Flask
from flask import render_template
import metrics
import review_history

# Start flask app
app = Flask(__name__)


@app.route("/app-reviews")
def index():
    queue_total = app_queue_data.total_queue()
    cloud_total = app_queue_data.cloud_queue()
    server_total = app_queue_data.server_queue()
    dc_total = app_queue_data.dc_queue()
    dual_total = app_queue_data.dual_apps()
    return render_template('app_reviews.html', title='Welcome', queue_numbers=queue_total, cloud_total=cloud_total, server_total=server_total, dc_total=dc_total, dual_total=dual_total)


@app.route("/total-queue")
def t_queue():
    full_queue = app_queue_data.data
    headers = ['Number', 'App Name', 'Partner', 'Platform', 'Submission Date']
    return render_template('total_queue.html', title='Queue Totals', app_queue=full_queue, headers=headers)


""" @app.route("/cloud-apps")
def c_queue():
    queue_total = app_queue_data.total_queue()
    return render_template('total_queue.html', title='Queue Totals', queue_number=queue_total) """


@app.route("/history")
def s_history():
    data = review_history.reviewHistory()
    data = data.controller()
    return render_template('submission_history.html', title='Submission History', app_tuple=data)


if __name__ == "__main__":
    print("Starting up QView")
    app_queue_data = metrics.metricMaker()
    #print(app_queue_data.data)

    app.run()