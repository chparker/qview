import queue_values as qv

class metricMaker(object):
    

    def __init__(self):

        print("Initialising metrics class")
        self.data = qv.controller()

    def total_queue(self):
        total_submissions = len(self.data)
        return total_submissions

    def cloud_queue(self):
        substring1 = "cloud"
        substring2 = "and"
        cloud_apps = self.platform_counter(substring1, substring2)
        return cloud_apps

    def server_queue(self):
        substring1 = "server"
        substring2 = "and"
        server_apps = self.platform_counter(substring1, substring2)
        return server_apps

    def dc_queue(self):
        substring1 = "data center"
        substring2 = "and"
        dc_apps = self.platform_counter(substring1, substring2)
        return dc_apps

    def dual_apps(self):
        substring = "and"
        dual_apps = self.platform_counter(substring)
        return dual_apps


    def platform_counter(self, *args):
        apps = []
        if len(args) == 2:
            substring1 = args[0]
            substring2 = args[1]
            for app in self.data:
                if substring1 in app[5] and substring2 not in app[5]:
                    apps.append(app[0])
        else:
            substring = args[0]
            for app in self.data:
                if substring in app[5]:
                    apps.append(app[0])
        return len(apps)

