import rest
from bs4 import BeautifulSoup
import lxml

app_url = "https://marketplace.atlassian.com/admin/review/apps"


# Controller function
def controller():
    print("Beginning data collection.")
    raw_page_content = collector(app_url)
    submissions = data_extractor(raw_page_content)
    clean_submissions = data_filter(submissions)
    final_list = add_id(clean_submissions) 
    return final_list


# Pull down the app review html page
def collector(url):
    app_page = rest.requestMaker(url)
    raw_page = app_page.get()
    return(raw_page.content)


# Extract only the values we want from the submissions table
def data_extractor(raw_data):
    table_values = []
    html = BeautifulSoup(raw_data, 'lxml')
    
    app_table = html.table
    table_rows = app_table.find_all('tr')
    for tr in table_rows[1:]:
        td = tr.find_all('td')
        td_href = tr.find_all('a')
        row1 = [i.text for i in td]
        row2 = [str(i) for i in td_href]
        app_key = app_key_extractor(row2)
        rewiew_ticket = review_ticket_collector(app_key)
        app_id = app_id_extractor(row2)
        version_build_number = buildnumber_extractor(row2)
        row1.extend([app_id, version_build_number, app_key, rewiew_ticket])
        table_values.append(row1)
    table_values_clean = [x for x in table_values if x != []]
    return table_values_clean


# Function to extract app ID
def app_id_extractor(href_td):
    app_id_pulled = href_td[1].split('/', 4)[-2]
    return app_id_pulled


# Function to extract version buildnumber
def buildnumber_extractor(href_td):
    v_build_num = href_td[1].split('/', 6)[-2]
    return v_build_num


# Strip out undeeded entries
def data_filter(data):
    stripped_data = [i.pop(6) for i in data]
    return data


def add_id(app_list):
    id_numb = 0
    for app in app_list:
        id_numb = id_numb + 1
        app.insert(0, id_numb)
    return app_list

def app_key_extractor(href_td):
    split_one = href_td[4].split('/', 4)[-1]
    split_two = split_one.split('/', 1)
    app_key = split_two[0]
    return app_key


def review_ticket_collector(app_key):
    url = f"https://marketplace.atlassian.com/rest/2/addons/{app_key}/approval"
    call_object = rest.requestMaker(url)
    response = call_object.get()
    content = response.json()
    app_key = content['_links']['issueInternal']['href'].split('/')[-1]
    return app_key



