import requests
import json
import authy

class requestMaker(object):

    auth = (authy.username, authy.password)

    def __init__(self, url):
        self.url = url
        pass

    def get(self):
        data = requests.get(self.url, auth=self.auth)
        print(data.status_code)
        return data

    def post(self):
        pass