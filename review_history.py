import rest

# Class to retrieve subnmission history from the last 7 days

class reviewHistory(object):

    def __init__(self):

        self.url = "https://ecosystem.atlassian.net/rest/servicedesk/reports/1/servicedesk/AMKTHELP/report/172"
        print("Initiating Review History class")
        self.controller()

    def controller(self):
        try:
            app_history = self.app_history()
        except Exception as err:
            raise err
        return app_history


    def app_history(self):
        request = rest.requestMaker(self.url)
        response = request.get()
        extracted_data = self.data_processor(response.json())
        return extracted_data


    def data_processor(self, data):
        tickets_created = data['series'][0]['seriesSummaryValue']
        tickets_resolved = data['series'][1]['seriesSummaryValue']
        combined_data = ({"tickets_created": f'{tickets_created}'}, {"tickets_resolved": f'{tickets_resolved}'})
        return combined_data



""" if __name__ == "__main__":
    reviewHistory() """